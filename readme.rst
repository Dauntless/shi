shi project
===========

The shi project is a vehicle to help anyone (the author included) learn the fundamentals of important computer
science topics, including but not limited to:

- Sorting algorithms
- Graphing problems
  - graph traversal
- Data Structures
  - Binary Search Trees
  - Linked Lists
  - Persistent Red Black Trees
  - Hash Tables
  - Heaps
- Software Engineering
  - Test Driven Development
  - Unit Test Coverage
  - Documentation


The idea is to create from scratch all those things covered in basic computer science courses, but this time with
some advanced coverage.  If you are like the author, it may have been several years since you have covered any
sorting algorithms or wrote some of your own data structures from scratch.  Believe it or not though, sometimes 
you do have to write your own data structures or figure out how to search for an item in a data structure.


Rationale
---------

After several failed job interviews, I realized that some 6+ years after college, I had forgotten a lot of the 
basics of computer science.  But I think that is what separates "programmers" from true engineers.  Being able to
recognize a problem is similar to a divide and conquer problem is somewhat similar to the "Design Patterns" that
are in vogue.  

A secondary motivation for this was to learn some new technologies or concepts.  For example, the data structures
here will progress from basic data structures, to fully thread-safe persistent data structures.  Eventually I plan
on using these C++ data structures in their own library.  In order to create the thread-safe and persistent data
structures, I am learning the new features in C++11.


C++11 coverage
--------------

There are quite a few new features in C++11 which I believe make it a much more viable candidate for solving problems
in a functional manner.  As such, many of the techniques I will be using will be using lambdas and templates instead
of trying to solve everything in an object oriented manner.  

Lambdas
+++++++

Lambdas are (to me) the new killer feature of C++11.  The ability to create function objects anywhere makes a more
functional approach to programming easier.  This combined with std\:\:function makes it even easier.  I will try
to demonstrate examples of lambdas with closures, and how they can be useful.

Thread API
++++++++++

C++11 now has a thread API included.  This means you only have to learn the C++11 thread API instead of one API per
operating system

Range based loops
+++++++++++++++++

C++11 now has the ability to easily traverse through containers without using iterators.  This makes iterating 
through STL containers even easier than before.  


Learning Tools
--------------

As well as prodigious documentation and commenting in the code, I am also creating an OpenGL based GUI to help 
visualize the data structures and operations on them.  I believe that having animations or visual representations
of a data structure greatly helps understand how a data structure and an algorithm operating on it works.

This very document is a reStructuredText document (used extensively in the python community).  It is a great way
to document high-level concepts as opposed to doxygen style API documentation.  While doxygen is great as an API
reference, it's not all that good for reference or tutorial work.


Building and Installing
-----------------------

The shi project uses waf as its build system.  While not a wide spread build tool, it has some advantages compared to
a build tool like CMake.  One of the strongest features is that it uses python for the builds.  That means anything
you can do in python, you can do in waf.  Need to dynamically download source code?  Need to connect to a database?
These are things you can't do with CMake.  So despite CMake's popularity, I decided to write the build tool using
waf.


Dependencies
++++++++++++

The waf build system will check for and notify you of any failed dependencies.  The shi project was tested and 
built on Fedora18, but should work on other systems.  Currently, only builds on linux are supported, but eventually
Windows, and perhaps Android will be supported.

Required:
-GMock
-GTest
-Boost

Optional:
-OpenGL



How to build
++++++++++++

The first step is to configure the build.  By default, it will pick clang as the compiler, but this can be 
overridden with the --compiler option.  Note that most shared libraries are built with gcc or g++, so you may run
into some linker problems if you decide to build the GUI tool.

The waf build will attempt to look for all the required development libraries and header files.  If any detected
development libraries are not found, the missing library will be displayed.  Eventually, an option to automatically
download the source tarballs of any dependencies will be written.

The dependencies will be searched for in the default locations, but these can be manually specified.  On many modern
linux systems, library paths are no longer searched for in LD_LIBRARY_PATH, but instead in folders indicated by
/etc/ld.so.conf (which in turn looks for .conf files in /etc/ld.so.conf.d).

To run the configuration simply run::

    ./waf configure

Assuming configuration has completed successfully, then you can build with the following command::

    ./waf build


Currently there is no install phase, but the binaries get generated in $PROJ_DIR/build/src.



