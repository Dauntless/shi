'''
This is the waf build script for shi
'''
import os
import urllib2

top = '.'
out = "build"


def notImpl(fn):
    def wrapper():
        print "TODO: {0} is not yet implemented".format(fn.__name__)
    return wrapper



def find_gmock(ctx):
    '''
    Find the gmock/gtest library
    '''
    print "trying to find Google gmock"
    if "GMOCK_HOME" in ctx.env:
        print "GMOCK_HOME is in env"
        return True

    has_gmock = False

    if ctx.options.gmock and \
       os.path.exists(ctx.options.gmock):
        has_gmock = True
        ctx.env['GMOCK_HOME'] = ctx.options.gmock
    else:
	print "ctx.options.gmock is ", ctx.options.gmock
    
    
    if not has_gmock:
        getGmock()
        ctx.fatal("Could not find gmock/gmock.h")
    
    return has_gmock



@notImpl
def getGmock(version):
    '''
    Will retrieve the google gmock source 
    '''
    pass



@notImpl
def find_boost(ctx):
    '''
    Searches for boost library
    '''
    pass




def configure(ctx):
    HOME = os.environ['HOME']
    has_gmock = find_gmock(ctx)
    if has_gmock:
        ctx.env.GMOCK_INC = ctx.env.GMOCK_HOME + "/include"
        ctx.env.GMOCK_LIB = ctx.env.GMOCK_HOME + "/lib"
        ctx.env.GTEST_HOME = ctx.env.GMOCK_HOME + "/gtest"
        ctx.env.GTEST_INC = ctx.env.GTEST_HOME + "/include"
        ctx.env.GTEST_LIB = ctx.env.GTEST_HOME + '/lib'

    ctx.find_program(ctx.options.compiler, var="CLANG", mandatory=True)
    ctx.env['CXX'] = [ctx.options.compiler]
    ctx.load("compiler_cxx")
    
   

def options(conf):
    conf.add_option("--compiler", 
                    action="store",
		    default="clang++",
		    help="compiler to use (clang++ or g++)")
    conf.add_option("--gmock",
                    type="string",
		    dest="gmock",
		    help="location of google gmock library or GMOCK_HOME env var")
    
		    
    conf.load("compiler_cxx")



def build(bld):
    bld.recurse('src')  ## call build from ./src/wscript

