
#include "templates/node.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <iostream>

using ::testing::AtLeast;
using SingleLinkedList::Node;
using std::cout;

TEST(NodeTestCreation, CreateAnIntNode) {
    Node<int> node;

    ASSERT_TRUE(node.data == 0);
    ASSERT_TRUE(node.right == nullptr);
}

TEST(NodeTestCreationOverload1, CreateAnIntNodeWithOverloadConstructor) {
    Node<int> node{10};

    ASSERT_TRUE(node.data == 10);
    ASSERT_TRUE(node.right == nullptr);
}


TEST(NodeTestCreationPointer, CreateIntNodePointer) {
    Node<int>* node = new Node<int>{5};
    
    ASSERT_TRUE(node->data == 5);
    ASSERT_TRUE(node->right == nullptr);
}


TEST(NodeTestCreateNode, TestInsertNodeFunction) {
    Node<int> node;
    
    ASSERT_TRUE(node.insertNode(10) == 0);
    ASSERT_TRUE(node.right->data == 10);
    ASSERT_TRUE(node.right->right == nullptr);

    node.right->insertNode(20);
    ASSERT_FALSE(node.right->right->data == 10);
    ASSERT_FALSE(node.right->right->right == node.right);
}


TEST(NodeTestFindMe, CheckIfNodeCanFindItself) {
    std::shared_ptr<Node<int> > head{ new Node<int>{5}};
    std::shared_ptr<Node<int> > ptrN = head;
    head->insertNode(10);
    ptrN = head->right;
    for( auto i : { 15, 20, 25, 30, 35 } ) {
	ptrN->insertNode(i);
	ptrN = ptrN->right;
    }    
    ptrN = head->right->right->right;
    
    auto rtn = ptrN->findMe(head);
    ASSERT_TRUE(rtn->data == 20);
    
}


TEST(NodeTestFindNode, MakeSureFindNodeFindsRightNode) {
    std::shared_ptr<Node<int> > head{ new Node<int>{5}};
    std::shared_ptr<Node<int> > ptrN = head;
    head->insertNode(10);
    ptrN = head->right;
    for( auto i : { 15, 20, 25, 30, 35 } ) {
	//cout << "ptrN is " << ptrN << std::endl;
	ptrN->insertNode(i);
	ptrN = ptrN->right;
    }    
    
    
    std::shared_ptr<Node<int>> node{ new Node<int>{100} };
    auto rtn = ptrN->findNode(ptrN, node);
    ASSERT_TRUE(rtn == nullptr);

    
    node = head->right->right->right;
    ptrN = head;
    rtn = node->findNode(ptrN, node);
    ASSERT_TRUE(rtn->data == node->data);
    ASSERT_TRUE(rtn->right == node->right);
    
    
}



TEST(NodeTestDeleteNode, MakeSureRightPointsToRightNode) {
    std::shared_ptr<Node<int> > head{new Node<int>{10}};
    std::shared_ptr<Node<int> > nright = head;

    for( auto i : { 12, 13, 14, 15} ) {
	nright->insertNode(i);
	nright = nright->right;
    }
    
    nright = head->right->right->right; // should have data == 13
    //ASSERT_TRUE(right->data == 13);

    //head->deleteNode(head, right);
    
}

int main(int argc, char** argv) {
    ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}

