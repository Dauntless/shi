#include "templates/bintree.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <iostream>
#include <random>

using ::testing::AtLeast;
using ::testing::Test;
using ::testing::Lt;
using ::testing::Gt;
using BinaryTree::Node;
using std::cout;


////////////////////////////////////////////////////////////////////////////////////
// Start fixtures

class RBTreeNodeInsertTest : public Test {
public:
    std::shared_ptr<Node<short>> parent_node;
    std::shared_ptr<Node<short>> node1;
    std::shared_ptr<Node<short>> node2;
    std::shared_ptr<Node<short>> node3;
    std::shared_ptr<Node<short>> node4;

    std::default_random_engine gen;
    std::vector<std::shared_ptr< Node<short> > > nodes;
    
    void SetUp();

    void insertNode(std::shared_ptr<Node<short>> p, 
		    std::shared_ptr<Node<short>> n);

    void insertNode(std::shared_ptr<Node<short>> p, short val,
	            std::function<int(short,short)> fnc);
}; 


void RBTreeNodeInsertTest::SetUp() {
    // create some randomizers
    std::uniform_int_distribution<int> dist{10,20};
    std::uniform_int_distribution<short> rndval{0,100}; 

    parent_node.reset( new Node<short>(rndval(gen)) );
    node1.reset(new Node<short>(rndval(gen)));
    node2.reset(new Node<short>(rndval(gen)));
    node3.reset(new Node<short>(rndval(gen)));
    node4.reset(new Node<short>(rndval(gen)));

    // Our comparison function object
    auto cmp = [](short parent_val, short new_val) -> int {
	return new_val - parent_val;
    };


    int num_nodes = dist(gen);
    for(int i=0; i < num_nodes; i++) {
	int rnd = rndval(gen);
	std::shared_ptr<Node<short>> nd{ new Node<short>(rnd, cmp) };
	nodes.push_back(nd);
    }
    
}

void RBTreeNodeInsertTest::insertNode(shared_ptr<Node<short>> p,
				      shared_ptr<Node<short>> n) {
    p->insert(n);
}


void RBTreeNodeInsertTest::insertNode(shared_ptr<Node<short>> p, 
				      short val,
                                      std::function<int(short, short)> fn) {
    p->insert(val, fn);
}




class RBTreeNodeDeleteTest : public RBTreeNodeInsertTest {
public:
    short deleteNode(shared_ptr<Node<short>> p, 
		     short val,
		     std::function<int(short, short)> fn);
};


short RBTreeNodeDeleteTest::deleteNode(shared_ptr<Node<short>> p,
				       short val,
				       std::function<int(short,short)> fn) {
    p->remove(val, fn);
}

// End Fixtures
/////////////////////////////////////////////////////////////////////////////////////


TEST(RBTreeNodeCreateTest, VerifyNodeIsCreated) {
    Node<short> node;
}

TEST_F(RBTreeNodeInsertTest, InsertThreeNodes) {
    auto cmp = [](short parent, short newnode) -> int {
	return newnode - parent;
    };
    
    std::shared_ptr<Node<short>> n1a = parent_node->insert(node1);
    std::shared_ptr<Node<short>> n2a = parent_node->insert(node2);
    std::shared_ptr<Node<short>> n3a = parent_node->insert(node3);
    
    
    std::weak_ptr<Node<short>> n1 = parent_node->insert(7, cmp);
    std::weak_ptr<Node<short>> n2 = parent_node->insert(15, cmp);
    std::weak_ptr<Node<short>> n3 = parent_node->insert(9, cmp);

    //ASSERT_TRUE(n3->parent == n1);
    //ASSERT_TRUE(n1 == node1);
    //ASSERT_TRUE(n2 == node2);
}


TEST_F(RBTreeNodeInsertTest, TestRandom) {
    for(auto n : nodes) {
	parent_node->insert(n);
    }

    // Define a function that captures the outer variables (eg. parent_node)
    // Then we can call it recursively
    
}


TEST_F(RBTreeNodeDeleteTest, DeleteThirdLevelNode) {

}



int main(int argc, char** argv) {
    ::testing::InitGoogleMock(&argc, argv);
    return RUN_ALL_TESTS();
}
