// Small test application
#include <iostream>
#include <memory>  // for smart pointers

using std::cout;

enum Color {
    RED = 0,
    BLACK = 1
};



namespace SingleLinkedList {



    //////////////////////////////////////////////////////////////
    // Node structure
    //////////////////////////////////////////////////////////////
    template<typename T>
    class Node {
    public:
	T data;
	std::shared_ptr<Node> right;
	std::shared_ptr<Node> left;



	Node();
	Node(T val);
	Node(T val, Node* n);
	virtual ~Node();
	Node(const Node& src);
	Node(Node&& src);
	Node& operator=(const Node& orig);

	int insertNode(T v);
	std::shared_ptr<Node<T> > findMe(std::shared_ptr<Node<T>> head);
	std::shared_ptr<Node<T> > findNode(std::shared_ptr<Node<T> > head,
					   std::shared_ptr<Node<T> > node);
	int deleteNode(std::shared_ptr<Node<T> > head, 
		       std::shared_ptr<Node<T> > node);
	
    };

    // Default constructor
    template<typename T>
    Node<T>::Node() : data(0), right(nullptr) {
        
    }

    // Constructor with value
    template<typename T>
    Node<T>::Node(T val) : data(val), right(nullptr) {
    
    }

    // TODO: Default destructor
    template<typename T>
    Node<T>::~Node() {
	//cout << "Deleting node with val" << data << std::endl;
    }

    // TODO: Move contructor
    template<typename T>
    Node<T>::Node(Node&& mv) {
	
    }


    // TODO: Copy constructor
    template<typename T>
    Node<T>::Node(const Node& orig) {
        std::shared_ptr<Node<T> > node{orig.data};
	node.right.reset(orig.right);
    }

    // Assignment operator
    template<typename T>
    Node<T>& Node<T>::operator=(const Node& orig) {
	std::shared_ptr<Node<T> > node{orig.data};
	node.right.reset(orig.right);

	return node;
    }

    template<typename T>
    int Node<T>::insertNode(T v) {
        if(this->right != nullptr)
	    return 1;  //FIXME: Hard coded magic constant
	this->right.reset(new Node<T>{v});
	return 0;
    }


    template<typename T>
    int Node<T>::deleteNode(std::shared_ptr<Node<T> > head, 
			    std::shared_ptr<Node<T> > node) {
	std::shared_ptr<Node<T> >  nright = head;
        cout << "in deleteNode\n";
	while(head != nullptr) {
            cout << nright << std::endl;
	    if(head->right == node) 
		break;
	    head = head->right;
	}
	if(head == nullptr) {
	    cout << "Could not find node\n";
	    return 1;
	}
	head->right = nright;
	
    }


    template<typename T>
    std::shared_ptr<Node<T> > 
    Node<T>::findNode(std::shared_ptr<Node<T> > head,
		      std::shared_ptr<Node<T> > node) { 
	std::shared_ptr<Node<T> > nright = head;
	cout << "In findNode\n";
        while( nright != nullptr ) {
	    cout << "nright = " << nright << ",head = " << head << ",node = " << node << std::endl;
	    if(nright == node)
		break;
	    nright = nright->right;
	    cout << "nright is now " << nright << std::endl;
	    
	}
	return nright;  //might return nullptr
    }
    


    template<typename T>
    std::shared_ptr<Node<T> > 
    Node<T>::findMe(std::shared_ptr<Node<T>> head) {
	std::shared_ptr<Node<T> > nright = head;
        while( nright != nullptr ) {
	    if(nright.get() == this)
		break;
	    // Go one forward so we can get one forward
	    nright = nright->right;
	}
	return nright;  //might return nullptr
    }
    


    template<typename T>
    class BinaryTree {
    public:
	Node<T> parent;
	Node<T> right;
	Node<T> left;
	
    };

} // end namespace SingleLinkedList

