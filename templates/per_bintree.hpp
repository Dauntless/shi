/**
 * This is a persistent version of the BinaryTree class.  
 */

#include "templates/bintree.hpp"

namespace Persistent {

    namespace PBinTree {

	enum Color {
	    RED = 0,
	    BLACK = 1
	};

	
	template<typename T>
	class PBinaryTree : public BinaryTree {
	public:
	    Color getColor();
	    void setColor(Color c);

	private:
	    Color color;
	    
	};

    }// end of PBinTree namespace
}// end of Persistent namespace
