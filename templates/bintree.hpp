#include <memory>
#include <vector>
#include <functional>
#include <iostream>
#include <mutex>

using std::shared_ptr;
using std::weak_ptr;
using std::cout;

namespace BinaryTree {


    /*
     * The BinaryTree::Node is a templated class which represents a simple binary Tree
     * Because it is a templated class, the type T must support proper copying 
     * (copy constructor and assignment operator).  If it is a pointer type,
     * it must also clean up after itself upon destruction.
     *
     * For the inner nodes, these are all shared pointers so that we don't have
     * to worry (as much) about freeing the memory in the Node.
     *
     * We derive this class from std::enable_shared_from_this to allow a shared
     * pointer from this.  Otherwise you would not be able to create a shared ptr
     * from the this object.
     */
    template<typename T>
    class Node : public std::enable_shared_from_this<Node<T>> {
    public:
	T data;
	shared_ptr<Node> parent;
	shared_ptr<Node> left;
	shared_ptr<Node> right;
	std::mutex l_lock;
	std::mutex r_lock;

	Node();
	~Node();
	Node(T val);
	Node(T val, std::function<int(T,T)> cmp);
	Node(const Node& cpy);
	Node(Node&& src);
	Node& operator=(const Node& cpy);

	/**
	 * Node insertion and deletion. Because this will be a thread safe data 
	 * structure, we have to protect these mutations.  It is possible that two
	 * or more threads might be searching for a node to insert or delete.
	 *
	 * Note that we have two guards: left and right.  If one thread is traversing
	 * a tree, the entire tree does not have to be locked.  It only needs to 
	 * acquire the left or right lock.
	 */
	shared_ptr<Node> insert(shared_ptr<Node> node);  // insert given node position
	weak_ptr<Node> insert(T val, std::function<int(T,T)> cmp);                  // insert given the value
	shared_ptr<Node> remove(shared_ptr<Node> node);  // look up and remove by node
	weak_ptr<Node> remove(T val, std::function<int(T,T)> cmp);

	/**
	 * fn is a function object (lambda, functor, etc)
	 */
	shared_ptr<Node> lookup(T key, std::function<int(T)> fn);

	/**
	 * A function object that provides the means to compare node objects
	 * This gives us the flexibility for comparing based on type T
	 */
        void setComp(std::function<int(T,T)> cmp) { comparator = cmp; };

	/**
	 * Sort the binary tree such that left node < right node
	 */
	void sort(std::function<int(T)> cmp);


	/**
	 * Create a sorted sequence from the binary tree
	 */
	template<typename S>
	S makeSortedSequence();

	
    private:
	// a function object comparator.  This is used to compare the 
	// type T data so that we can properly sort and balance the tree
	// It is also used to help look up a value
	std::function<int(T,T)> comparator;
    };


    template<typename T>
    Node<T>::Node() {
	comparator = nullptr;
    }


    template<typename T>
    Node<T>::~Node() {
	cout << "Calling destructor on " << this << std::endl;
	cout << "\tthis->data = " << this->data << std::endl;
	cout << "\tthis->left = " << this->left << std::endl;
	cout << "\tthis->right = " << this->right << std::endl;
    }


    template<typename T>
    Node<T>::Node(T val) {
	data = val;
	parent = nullptr;
	left = nullptr;
	right = nullptr;

	comparator = nullptr;
    }


    /**
     * Notice the syntax of the std::function.  It is 
     * std::function<return_t(arg1_t,...)> fnc;
     */
    template<typename T>
    Node<T>::Node(T val, std::function<int(T,T)> cmp) : 
	Node(val) {
        comparator = cmp;
    }


    /**
     * Copy constructor
     */
    template<typename T>
    Node<T>::Node(const Node& cpy) {
	data = cpy.data;
	parent = cpy.parent;
	left = cpy.left;
	right = cpy.right;

	comparator = cpy.comparator;
    }


    /**
     * Move Constructor
     */
    template<typename T>
    Node<T>::Node(Node&& src) {
	
    }


    template<typename T>
    Node<T>& Node<T>::operator=(const Node& rhs) {
	data = rhs.data;
	parent = rhs.parent;
	left = rhs.left;
	right = rhs.right;
	comparator = rhs.comparator;
    }

    

    /**
     * Simple binary tree insertion where values which compare as less than
     * go down the left path, and values which compare as greater than go
     * down the right path.  When the proper location is found to insert, a
     * shared_ptr<Node<T>> is created that contains val, and inserted in the
     * node.
     */
    template<typename T>
    weak_ptr<Node<T>> Node<T>::insert(T val, std::function<int(T,T)> cmp) {
	// TODO: handle case where this->data == val
	int relation = cmp(this->data, val);

	if(relation == 0) {
	    weak_ptr<Node<T>> me = this->shared_from_this();
	    return me;
	}

	if(relation < 0) {
	    if(this->left == nullptr) {
		this->left.reset( new Node<T>(val, cmp) );
		weak_ptr<Node<T>> me = this->left;
		//this->left->parent = this->shared_from_this();
		return me;
	    }
	    return this->left->insert(val, cmp);
	}
	else {
	    if(this->right == nullptr) {
		this->right.reset( new Node<T>(val, cmp) );
		weak_ptr<Node<T>> me = this->right;
		//this->right->parent = this->shared_from_this();
		return me;
	    }
	    return this->right->insert(val, cmp);
	}
    }


    /**
     * Recursive function to find a node and remove it.  It must "reattach" any children nodes.
     */
    template<typename T>
    weak_ptr<Node<T>> Node<T>::remove(T val, std::function<int(T,T)> cmp) {
	
    }



    /**
     * Implements a simple binary insert.  
     */
    template<typename T>
    shared_ptr<Node<T>> Node<T>::insert(std::shared_ptr<Node<T>> n) {
	if(comparator == nullptr)
	    return nullptr;  // FIXME: need error codes
	
	int relation = comparator(this->data, n->data);
	if(relation == 0) {
	    return this->shared_from_this();
	}

	/**
	 * Search the left tree.  If comparator returns a negative number, then
	 * the value is less than the current node.  Check if this->left is empty
	 * and if not, this->left is assigned the node. If it isn't empty, then
	 * recursively descend down the left node.
	 */
	if(relation < 0) {
	    if(this->left == nullptr) { // if left is empty, insert n
		cout << "Inserting " << n << ":" << n->data << " to this->left\n";
		this->left = n;
		return this->left;
	    }
	    else 
		return this->left->insert(n);
	}
	/**
	 * Search the right tree.  Same as above, but if comparator returns a
	 * positive value, then go down the right path.
	 */
        else {
	    if(this->right == nullptr) {
		cout << "Inserting " << n << ":" << n->data << " to this->right\n";
		this->right = n;
		return this->right;
	    }
	    else 
		return this->right->insert(n);
	}
    }


    
    

} // end of RBTree namespace
